<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Faker\Factory;
use App\Models\Tweet;
use App\User;
use App\Models\Comment;
use App\Models\Contact;


// Route of About page notice here it points to the AboutController @ means it will be calling the function create in the AboutController
Route::get('/about', 'AboutController@create');


Route::get('/contact', 'ContactController@create');
Route::post('/contact', 'ContactController@store');

Route::get('/tweet', 'TweetController@create');
Route::post('/tweet', 'TweetController@store');

Route::get('/tweet/{id}', 'TweetController@userTweets');

Route::get('/tweets/{id}/like/toggle', 'TweetController@toggleLike');

Route::get('/', 'MainController@load');
Route::post('/', 'MainController@store');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
