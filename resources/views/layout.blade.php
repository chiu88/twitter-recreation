<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Laravel-example</title>
        <link rel="stylesheet" href="/css/app.css">
    </head>

    <?php if($errors->any()):    ?>
        <div class="alert alert-danger">
            <ul>
                <?php foreach ($errors->all() as $error): ?>
                    <li>{{$error}}</li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>

    <div class="header-container">
        @if (Auth::check())
            Hello, {{ Auth::user()->name }}
            <a href="{{ route('logout') }}"
                onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();">
                Logout
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        @else

            <a href="/login">Login</a>
        @endif

        <header class="header">
            <a href="/">Home</a>
            <a href="/contact">Contact</a>
            <a href="/tweet">Tweet</a>
            <a href="/register">Register</a>
        </header>
    </div>


    <body>

        @yield('content')
    </body>
</html>
