
<div class="col-sm-3 form-group">
    <?php echo $label ?>  <br>
    <input type="text" name="<?php echo $name ?>" value="<?php echo old($name) ?>" class='form-control <?php echo $errors->has($name) ? 'is-invalid' : ''; ?>'>
    <?php if($errors->has($name)): ?>
        <span class='invalid-feedback'> <?php echo $errors->first($name) ?></span>
    <?php endif; ?>
    <br>
</div>
