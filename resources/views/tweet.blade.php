@extends('layout')

@section('content')

<!-- this is only needed if we want to display something to the user -->
<?php if($errors->any()):    ?>
    <div class="alert">
        <ul>
            <?php foreach ($errors->all() as $error): ?>
                <li><?php echo $error ?></li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>


<h1>Create a Tweet</h1>
<form method="post">
    <?php echo csrf_field() ?>
    Write a tweet here: <br>
    <textarea name="tweet" rows="8" cols="80"></textarea>
    <input type="submit" name="" value="Submit">
</form>
@endsection
