@extends('layout')

@section('content')

<?php if($errors->any()):    ?>
    <div class="alert">
        <ul>
            <?php foreach ($errors->all() as $error): ?>
                <li><?php $error ?></li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>

    <div class="body-section flex flex-horizontal">

        <div class="space-section">

        </div>

        <div class="tweet-section">
            <div class="tweet-nav flex flex-horizontal">
                <a href="">Tweets</a>
                <a href="">Tweets and Replies</a>
                <a href="">Media</a>
            </div>
            <hr>
            <ul class="tweet-container">
                <?php foreach($tweets as $tweet): ?>
                    <li>

                        <div class="info">
                            <img class='images' src="<?php echo $tweet->user->image ?>">
                            <div class="user-info">
                                <a href="./tweet/{{$tweet->user->id}}"><?php echo $tweet->user->name ?></a>
                                @<?php echo $tweet->user->handle ?>
                                <?php echo $tweet->date ?>
                            </div>
                        </div>
                        <div class="content-container">
                            <div class='content'><?php echo $tweet->content ?></div><br>
                            This tweet liked by <?php echo count($tweet->likes) ?> users.
                            <div class="dateJoined"><?php echo $tweet->date ?></div>


                            <br>
                            Comments: <br>
                            <form method="post">
                                <?php echo csrf_field() ?>
                                <input type="text" name="tweetComment" placeholder="Comment on this tweet!">
                                <input type="hidden" name="tweetId" value="<?php echo $tweet->id ?>">
                                <input type="submit" name="" value="Submit">
                            </form>
                            <br>

                            <?php foreach($tweet->comments as $comment): ?>
                                <strong><?php echo $comment->user->name ?> @<?php echo $comment->user->handle ?></strong> says: <br>
                                <?php echo $comment->content ?><br>

                            <?php endforeach; ?>
                            <br><br>
                            Retweets <?php echo $tweet->retweets ?>




                            @if ($tweet->isLikedByCurrentUser())
                                <a href="/tweets/{{ $tweet->id }}/like/toggle">unlike</a>
                            @else
                                <a href="/tweets/{{ $tweet->id }}/like/toggle">like</a>
                            @endif


                            <br><br>

                        </div>
                    </li>

                <?php endforeach; ?>

            </ul>
        </div>

        <div class="signup-user-container">

            <div class="signup-section">
                <div class="signup-headers">
                    <h2>New to twitter?</h2>
                    <h6>Sign up now to get your own personalized timeline!</h6>
                </div>
                <div class="signup-button-container">
                    <button class="signup-button" type="button" name="button" onclick="window.location.href='/register'">Sign up</button>
                </div>

            </div>

            <div class="user-section">


                <h3>You may also like:</h3>

                <ul>
                    <?php foreach($users as $user): ?>
                        <li>
                            <img class='images-profile' src="<?php echo $user->image ?>"><br>
                            <a href="./tweet/{{$user->id}}"><?php echo $user->name ?></a>
                            <br>
                            @<?php echo $user->handle ?>
                        </li>

                    <?php endforeach; ?>

                </ul>
            </div>
        </div>
    </div>



</body>
@endsection
