@extends('layout')

@section('content')
    <h1>{{ $user->name }}</h1>

    <ul>
        @foreach ($tweets as $tweet)
            <li>{{ $tweet->content }}</li>
        @endforeach

        
    </ul>
@endsection
