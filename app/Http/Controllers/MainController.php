<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Faker\Factory;
use App\Models\Tweet;
use App\User;
use App\Models\Comment;
use App\Models\Contact;

class MainController extends Controller
{
    //This is where we populate the database
    function load() {
        $faker = Factory::create();

        // $user1 = new User();
        // $user1->name = $faker->name;
        // $user1->handle = 'realDonaldTrump';
        // $user1->image = $faker->imageUrl;
        // $user1->dateJoined = date_create();
        //
        // $user2 = new User();
        // $user2->name = $faker->name;
        // $user2->handle = 'realBarrackObama';
        // $user2->image = $faker->imageUrl;
        // $user2->dateJoined = date_create()->modify('-1 year');

        $users = User::all();

        $tweets = Tweet::all();

        $comments = Comment::all();

        // $tweet1 = new Tweet();
        // $tweet1->image = $faker->imageUrl;
        // $tweet1->name =$faker->name;
        // $tweet1->handle = 'realDonaldTrump';
        // $tweet1->time = '12/18/2017 8:59pm';
        // $tweet1->content = $faker->paragraph;
        // $tweet1->comments = '23K';
        // $tweet1->retweets = '12K';
        // $tweet1->likes = '40K';
        //
        // $tweet2 = new Tweet();
        // $tweet2->image = $faker->imageUrl;
        // $tweet2->name =$faker->name;
        // $tweet2->handle = 'realBarrackObama';
        // $tweet2->time = '12/14/2017 6:59pm';
        // $tweet2->content = $faker->paragraph;
        // $tweet2->comments = '32K';
        // $tweet2->retweets = '445K';
        // $tweet2->likes = '1.6M';




        $data = [
            'users' => $users,
            'tweets' => $tweets,
            'comments' => $comments,

        ];

        return view('welcome', $data);
    }

    function store() {
        // user validation
        $request = request();
        $result = $request->validate(
            ['tweetComment' => 'required']
        );

        // the function store will post the data into the database, in our route file, we have specified that this function runs in the post route
        $data = request()->all();
        $loggedInUser = $request->user();

        $comment = new Comment();

        $comment->tweet_id = $data['tweetId'];
        $comment->user_id = $loggedInUser->id; 
        $comment->content = $data['tweetComment'];
        $comment->save();

        return redirect('/')->with('message', 'Your comment was successfully posted!');
    }

}
