<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contact;

/**
 *
 */
class ContactController extends Controller
{

    function create() {



        return view('contact');
    }

    function store() {
        $request = request();
        $result = $request->validate(
            [ 'firstName' => 'required|max:255',
             'lastName' => 'required|max:255',
             'phoneNumber' => 'required|regex:/[0-9]{9}/',
             'email' => 'required|max:255' ], ['phoneNumber.regex' => 'Please enter phone number in the format 4031112222']
        );

        $data = request()->all();
        $contact = new Contact();
        $contact->first_name = $data['firstName'];
        $contact->last_name = $data['lastName'];
        $contact->email = $data['email'];
        $contact->phone_number = $data['phoneNumber'];
        $contact->save();

        return redirect('/contact')->with('message', 'Your contact was successfully processed. Someone will be in touch with you soon');
    }

}


 ?>
