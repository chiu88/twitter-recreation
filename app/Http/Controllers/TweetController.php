<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tweet;
use App\User;

class TweetController extends Controller
{
    //
    function create() {
        // setcookie('bob', 'hahahahaha', time()+1555);
        // setcookie('bob', 'hahahahaha', time()-3000);
        // session_start();

        // $_SESSION['foo'] = 'bar';
        // session_destroy();
        //
        // dd($_SESSION);

        // this sets the session values, //

        // session_start();
        // session([
        //     'foo' => 'bar',
        //     'fools' => 'bats'
        // ]);
        // session_start([
        //     'cookie_lifetime' => 10,
        // ]);
        // session([
        //     'shawn' => '3',
        //
        // ]);

        // this retrieves the session data
        // $data = request()->all();
        // $tweet = new Tweet();
        // $tweet->user_id = session('shawn');
        // dd($tweet->user_id);
        if (!\Auth::check()) {
            return redirect('/login');
        }

        return view('tweet');
    }

    function store() {
        if (!\Auth::check()) {
            return redirect('/login');
        }

        // user validation
        $request = request();
        $result = $request->validate(
            ['tweet' => 'required|max:255'],

            ['tweet.max' => 'Please enter a tweet smaller than 255 characters']
        );

        $loggedInUser = $request->user();

        // the function store will post the data into the database, in our route file, we have specified that this function runs in the post route

        $data = request()->all();

        $tweet = new Tweet();
        $tweet->user_id = $loggedInUser->id;
        $tweet->content = $data['tweet'];
        $tweet->save();



        return redirect('/')->with('message', 'Your tweet was successfully posted!');
    }

    function userTweets($userId)
    {
        $user = User::where('handle', $userId)
            ->orWhere('id', $userId)
            ->first();

        if (!$user) {
            abort(404);
        }

        $tweets = $user->tweets;

        return view('userTweets', [
            'user' => $user,
            'tweets' => $tweets
        ]);
    }

    public function toggleLike($tweetId)
    {
        $user = request()->user();
        $tweet = Tweet::find($tweetId);

        if ($tweet->isLikedByCurrentUser()) {
            $tweet->likes()->detach($user);
        } else {
            $tweet->likes()->attach($user);
        }

        return back()
            ->with('message', 'You successfully liked a tweet.');
    }
}
